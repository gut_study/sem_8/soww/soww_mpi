MPICC=mpicc
AR=ar
CFLAGS=-O2 -Wall -Werror
ARFLAGS=-rc
LIBS=-lm
OBJ = soww_mpi.o

ifneq ($(filter lab%, $(firstword $(MAKECMDGOALS))),)
  LAB := $(firstword $(MAKECMDGOALS))
  # use the rest as arguments for "run"
  LAB_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(LAB_ARGS):;@:)
endif

%.o: %.c $(DEPS)
	${MPICC} -c -o $@ $< ${LIBS} ${CFLAGS}

soww_mpi: $(OBJ)
	${AR} ${ARFLAGS} libsowwmpi.a $^

clean:
# run only if clean is called without lab target
ifeq ($(LAB),)
	rm libsowwmpi.a soww_mpi.o
endif

lab%: FORCE
	cd $@ && $(MAKE) CFLAGS=-L.. LIBS=-lsowwmpi $(LAB_ARGS)
FORCE:
