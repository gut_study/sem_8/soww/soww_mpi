# SOWW MPI library

## Integration into lab

* Copy lab sources in subfolder `lab<n>`
* Patch two lines in `lab<n>/Makefile`
    ```diff
    -CFLAGS=-O2
    -LIBS=-lm
    +override CFLAGS += -O2
    +override LIBS += -lm
    ```
* add library header `#include "../soww_mpi.h"` into `lab<n>/mpi.c`
* If you run app in docker as root add `--allow-run-as-root` to `mpirun` in
  `run` target

## Usage

* build soww_mpi library
  ```
  make
  ```
* build `lab<n>` app
  ```
  make lab<n>
  ```
* run `lab<n>` app
  ```
  make lab<n> run <args>
* clean `lab<n>` app
  ```
  make lab2 clean
  ```
* clean `soww_mpi` lib
  ```
  make clean
  ```
