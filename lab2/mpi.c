#include "utility.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <mpi.h>
#include <time.h>
#include <string.h>

// comment/uncomment to switch debugging
// #define DEBUGGING
#include "../soww_mpi.h"

#define PACKAGE_SIZE 50
#define MAX_RANGE 1000
#define DATA 0
#define RESULT 1
#define FINISH 2
#define FREE 3
#define MIN_INT(x, y) ((x) < (y) ? (x) : (y))

void print_ints(int *ints, size_t size)
{
	for (int i = 0; i < size; i++)
	{
		printf("%d ", ints[i]);
	}
}
int main(int argc, char **argv)
{

	Args ins__args;
	parseArgs(&ins__args, &argc, argv);

	// program input argument
	long inputArgument = ins__args.arg; 

	struct timeval ins__tstart, ins__tstop;

	// int SOWW_myrank();, nproc;

	int result[MAX_RANGE + 1];
	memset(result, 0, (MAX_RANGE + 1) * sizeof(*result));

	int localresult[MAX_RANGE + 1];
	memset(localresult, 0, (MAX_RANGE + 1) * sizeof(*localresult));

	int *numbers;

	MPI_Status status;

	SOWW_init(argc, argv);

	if (!SOWW_myrank())
	{
		// **** MASTER ****
		// number of ints already sent to slaves
		int numbersSent = 0;

		// generate random ints
		srand(time(NULL));
		numbers = malloc(inputArgument * sizeof(int));
		for (int i = 0; i < inputArgument; i++)
		{
			numbers[i] = rand() % (MAX_RANGE + 1);
		}
		DEBUG
		{
			printf("\n\nRandom numbers generated\n\n");
			print_ints(numbers, inputArgument);
			printf("\n");
			fflush(stdout);
		}

		// distribute divided packages of random ints to slaves
		do
		{
			FOR_EACH_SLAVE_UNTIL(numbersSent < inputArgument)
			{
				int amountToSend = MIN_INT(inputArgument - numbersSent, PACKAGE_SIZE);

				int *package = numbers + numbersSent;
				DEBUG
				{
					printf("\n\nDATA sent\n");
					print_ints(package, amountToSend);
					printf("\n\n");
					fflush(stdout);
				}
				MPI_Send(package, amountToSend, MPI_INT, SLAVE_NR, DATA, MPI_COMM_WORLD);
				numbersSent += amountToSend;
			}
		} while (numbersSent < inputArgument);

		// Close slaves
		FOR_EACH_SLAVE
		{
			MPI_Send(NULL, 0, MPI_INT, SLAVE_NR, FINISH, MPI_COMM_WORLD);
		}
	}
	else
	{
		// **** SLAVE ****
		int package[PACKAGE_SIZE];
		do
		{
			int numbersOfElementsReceived;
			// get next message info
			MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			if (status.MPI_TAG == DATA)
			{
				// get data size
				MPI_Get_count(&status, MPI_INT, &numbersOfElementsReceived);
				// receive data
				MPI_Recv(package, numbersOfElementsReceived, MPI_INT, 0, DATA, MPI_COMM_WORLD,
						 &status);
				DEBUG
				{
					printf("\n\n[%d]: RECEIVED NUMBERS: ", SOWW_myrank());
					print_ints(package, numbersOfElementsReceived);
					printf("\n\n");
				}
				// sum the occrrences
				for (int i = 0; i < numbersOfElementsReceived; i++)
				{
					localresult[package[i]]++;
				}
			}
		} while (status.MPI_TAG != FINISH);
	}

	// synchronize/finalize your computations
	MPI_Reduce(&localresult, &result, MAX_RANGE, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

	if (!SOWW_myrank())
	{
		printf("HISTOGRAM:\n");
		for (int i = 0; i < MAX_RANGE; i++)
		{
			if (result[i] != 0)
			{
				printf(" (%d: %d) ", i, result[i]);
				if (i != 0 && i % 12 == 0)
					printf("\n");
			}
		}
	}

	MPI_Finalize();
}
