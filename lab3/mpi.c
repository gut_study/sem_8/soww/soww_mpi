#include "utility.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>
#include <mpi.h>

// #define DEBUGGING
#include "../soww_mpi.h"

#define PRECISION 0.000001
#define RANGESIZE 1
#define DATA 100
#define RESULT 101
#define FINISH 102
#define MAXRANGE 1000
#define ELEMENTS MAXRANGE + 1
#define PACKSIZE 1000

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

int main(int argc, char **argv)
{

    Args ins__args;
    parseArgs(&ins__args, &argc, argv);

    // program input argument
    int inputArgument = ins__args.arg;

    int myrank, nproc, numberOfWorkers;
    int final_result[ELEMENTS] = {0}, temp_result[ELEMENTS] = {0};

    SOWW_init(argc, argv);
    myrank = SOWW_myrank();
    numberOfWorkers = SOWW_nproc() - 1;

    if (myrank == 0)
    // #########################################################################
    // #                                MASTER                                 #
    // #########################################################################
    {
        struct SOWW_WorkerList *workerList = SOWW_create_worker_list();
        int numbers_sent = 0;
        DEBUG
        {
            fflush(stdout);
        }
        // Requests
        // sendRequests - requests used for sending data packages to workers
        //   - one for each worker
        //          -------------
        //   Worker | 1 | 2 | 3 |
        //   sendRq | 0 | 1 | 2 |
        //          -------------
        // reduceRequest - request to gather results from Workers to Master
        MPI_Request *sendRequests = SOWW_create_requests(numberOfWorkers);
        MPI_Request reduceRequest = MPI_REQUEST_NULL;
        {
            int req_no = 0;
            FOR_EACH_WORKER_NODE(node, workerList)
            {
                // TODO: to complex, need to simplify
                SOWW_Worker_add_request(&node->worker, &sendRequests[req_no]);
                req_no++;
            }
        }
        DEBUG
        {
            printf("\n Starting MASTER process\n");
            fflush(stdout);
        }

        // Generate random numbers
        int *generated_numbers = malloc(sizeof(int) * inputArgument);
        for (int i = 0; i < inputArgument; i++)
            generated_numbers[i] = rand() % (ELEMENTS);
        DEBUG
        {
            printf("Numbers generated\n");
            fflush(stdout);
        }

        // Send first packages to all slaves
        FOR_EACH_WORKER_NODE(node, workerList)
        {
            if (numbers_sent >= inputArgument)
                break;

            // Calculate amount of elements to send
            int number_of_elements = MIN(inputArgument - numbers_sent, PACKSIZE);

            MPI_Isend(generated_numbers + numbers_sent,
                      number_of_elements,
                      MPI_INT,
                      node->worker.rank,
                      DATA, MPI_COMM_WORLD,
                      SOWW_Worker_get_request(&node->worker, 0));
            numbers_sent += number_of_elements;
        }
        DEBUG
        {
            printf("First packages sent\n");
            fflush(stdout);
        }

        // Balance computation load between workers.
        // Send next data package to workers which received last one.
        int request_completed;
        while (numbers_sent < inputArgument)
        {
            // Wait for any worker process to receive data package
            MPI_Waitany(numberOfWorkers, sendRequests, &request_completed, MPI_STATUS_IGNORE);
            int number_of_elements = MIN(inputArgument - numbers_sent, PACKSIZE);

            // Send next data pacakge to a free worker
            MPI_Isend(generated_numbers + numbers_sent,
                      number_of_elements,
                      MPI_INT,
                      request_completed + 1,
                      DATA, MPI_COMM_WORLD,
                      &(sendRequests[request_completed]));
            numbers_sent += number_of_elements;
        }

        // Send finalize command to all workers
        FOR_EACH_WORKER_NODE(node, workerList)
        {
            MPI_Isend(NULL,
                      0,
                      MPI_INT,
                      node->worker.rank,
                      FINISH,
                      MPI_COMM_WORLD,
                      SOWW_Worker_get_request(&node->worker, 0));
            DEBUG
            {
                printf("\n[Master]: finish to slave %d sent\n", node->worker.rank);
                fflush(stdout);
            }
        }

        // Gather results from all workers
        MPI_Ireduce(&temp_result, &final_result, ELEMENTS, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD, &reduceRequest);
        DEBUG
        {
            printf("\n[Master]: Ireduce opened\n");
            fflush(stdout);
        }

        // Wait for reduce operation to finish
        // at this point all other messages should be received
        MPI_Wait(&reduceRequest, MPI_STATUS_IGNORE);

        // Master exit
    }
    else
    // #########################################################################
    // #                                WORKER                                 #
    // #########################################################################
    {
        MPI_Request requests[2];
        MPI_Status status;

        // request[0] - receive request
        // request[1] - reduce request
        requests[0] = requests[1] = MPI_REQUEST_NULL;
        int temp_tab[PACKSIZE];

        // Open first receive request
        MPI_Irecv(temp_tab, PACKSIZE, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &(requests[0]));
        do
        {
            DEBUG
            {
                printf("\n[Slave: %d]: Waiting for data\n", myrank);
                fflush(stdout);
            }

            // Wait for data receive
            MPI_Wait(&(requests[0]), &status);
            if (status.MPI_TAG == DATA)
            {
                int number_of_elements;
                MPI_Get_count(&status, MPI_INT, &number_of_elements);
                DEBUG
                {
                    printf("\n[Slave: %d]: data received, count: %d\n", myrank, number_of_elements);
                    fflush(stdout);
                }
                for (int i = 0; i < number_of_elements; i++)
                {
                    int value = temp_tab[i];
                    temp_result[value] += 1;
                }
                /* Open next receive request
                Ideally it should be done right after MPI_Wait and before our
                computations to unblock Master process. In order to do this, we
                would need to do memcpy of the shared buffer(temp_tab) into a
                new local buffer, so it is not overriden by MPI when a next
                message arrives - which has almost the same complexity as our
                computation - so no point in this.
                 */
                MPI_Irecv(temp_tab, PACKSIZE, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &(requests[0])); // #2
            }
            // If received the FINISH TAG, finalize process.
        } while (status.MPI_TAG != FINISH);

        // Send computation result to master to be summarized with other Worker's results
        MPI_Ireduce(&temp_result, &final_result, ELEMENTS, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD, &(requests[1]));
        DEBUG
        {
            printf("\n[Slave: %d]: Finished!, waiting for reduce\n", myrank);
            fflush(stdout);
        }
        // Do not exit before reduce finishes
        MPI_Wait(&requests[1], MPI_STATUS_IGNORE);
        DEBUG
        {
            int received_elements = 0;
            for (int i = 0; i < ELEMENTS; i++)
            {
                received_elements += temp_result[i];
            }
            printf("\n[Slave: %d]: Reduced!, received elements: %d\n", myrank, received_elements);
            fflush(stdout);
        }
        // Worker exit
    }

    // synchronize/finalize your computations
    if (!myrank)
    {
        // MASTER
        for (int i = 0; i <= MAXRANGE; i++)
        {
            printf("%d ", final_result[i]);
        }
        int sum = 0;
        for (int i = 0; i <= MAXRANGE; i++)
        {
            sum += final_result[i];
        }
        printf("\n%d %d\n", sum, inputArgument);
    }

    // #########################################################################
    // #                                COMMON                                 #
    // #########################################################################
    fflush(stdout);
    MPI_Finalize();
    return 0;
}
