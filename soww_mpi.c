#include "stdlib.h"

// #define DEBUGGING
#include "./soww_mpi.h"

static int _myrank, _nproc;

void SOWW_init(int argc, char **argv)
{
    MPI_Init(&argc, &argv);
    // obtain my rank
    MPI_Comm_rank(MPI_COMM_WORLD, &_myrank);
    // and the number of processes
    MPI_Comm_size(MPI_COMM_WORLD, &_nproc);
    DEBUG
    {
        printf("\n\nNPROC: %d\n\n", _nproc);
        fflush(stdout);
    }
}

int SOWW_myrank()
{
    return _myrank;
}

int SOWW_nproc()
{
    return _nproc;
}

int SOWW_worker_number()
{
    return _nproc - 1;
}
struct SOWW_WorkerList *SOWW_create_worker_sublist(size_t worker_count)
{
    static int worker_rank = 1;
    if (worker_count <= 0)
    {
        worker_rank = 0;
        return NULL;
    }
    struct SOWW_WorkerList *workerList = (struct SOWW_WorkerList *)malloc(sizeof(struct SOWW_WorkerList));
    workerList->worker = (struct SOWW_Worker){.rank = worker_rank, .requests = NULL, .requestCount = 0};
    worker_rank++;
    workerList->next = SOWW_create_worker_sublist(worker_count - 1);
    return workerList;
}
struct SOWW_WorkerList *SOWW_create_worker_list()
{
    return SOWW_create_worker_sublist(SOWW_worker_number());
}

MPI_Request *SOWW_create_requests(size_t request_count)
{
    int numberOfWorkers = SOWW_worker_number();

    MPI_Request *requests = (MPI_Request *)malloc(numberOfWorkers * sizeof(MPI_Request));
    for (int i = 0; i < numberOfWorkers; i++)
    {
        requests[i] = MPI_REQUEST_NULL;
    }
    return requests;
}

struct SOWW_RequestList *SOWW_RequestList_add(struct SOWW_RequestList *list, MPI_Request *request)
{
    struct SOWW_RequestList *lastNode = list;

    while (lastNode->next != NULL)
    {
        lastNode = lastNode->next;
    }

    lastNode->next = (struct SOWW_RequestList *)malloc(sizeof(struct SOWW_RequestList));
    lastNode->next->request = request;
    return list;
}

struct SOWW_RequestList *SOWW_RequestList_init(MPI_Request *requests, size_t requestCount)
{
    if (requestCount <= 0)
    {
        return NULL;
    }

    struct SOWW_RequestList *list = (struct SOWW_RequestList *)malloc(sizeof(struct SOWW_RequestList));
    // assign first request to head
    list->request = &requests[0];
    for (int i = 1; i < requestCount; i++)
    {
        SOWW_RequestList_add(list, &requests[i]);
    }
    return list;
}

MPI_Request *SOWW_RequestList_get(struct SOWW_RequestList *list, int index)
{
    struct SOWW_RequestList *desiredNode = NULL;
    int i = 0;
    FOR_EACH_REQUEST_NODE(node, list)
    {
        if (i == index)
        {
            desiredNode = node;
        }
        i++;
    }
    return desiredNode != NULL ? desiredNode->request : NULL;
}

void SOWW_Worker_add_request(struct SOWW_Worker *worker, MPI_Request *request)
{
    if (worker->requests == NULL)
    {
        worker->requests = SOWW_RequestList_init(request, 1);
    }
    else
    {
        SOWW_RequestList_add(worker->requests, request);
    }
    worker->requestCount++;
}

MPI_Request *SOWW_Worker_get_request(struct SOWW_Worker *worker, int index)
{
    if (worker->requestCount <= index)
        return NULL;

    return SOWW_RequestList_get(worker->requests, index);
}
