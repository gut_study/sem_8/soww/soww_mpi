#ifndef SOWW_MPI_HEADER
#define SOWW_MPI_HEADER

#include <mpi.h>
#include <stdio.h>

// define DEBUGGING to enable debug
#ifdef DEBUGGING
#define DEBUG if (1)
#else
#define DEBUG if (0)
#endif

#define FOR_EACH_SLAVE for (int SLAVE_NR = 1; SLAVE_NR < SOWW_nproc(); SLAVE_NR++)
#define FOR_EACH_SLAVE_UNTIL(x) for (int SLAVE_NR = 1; SLAVE_NR < SOWW_nproc() && (x); SLAVE_NR++)

#define FOR_EACH_WORKER_NODE(node, list) \
    for(struct SOWW_WorkerList *node = list; node != NULL; node = node->next)

#define FOR_EACH_REQUEST_NODE(node, list) \
    for(struct SOWW_RequestList *node = list; node != NULL; node = node->next)

struct SOWW_RequestList {
    MPI_Request *request;
    struct SOWW_RequestList *next;
};

struct SOWW_Worker {
    int rank;
    struct SOWW_RequestList *requests;
    size_t requestCount;
};

struct SOWW_WorkerList {
    struct SOWW_Worker worker;
    struct SOWW_WorkerList *next;

};

void SOWW_init(int argc, char **argv);

int SOWW_myrank();
int SOWW_nproc();

struct SOWW_WorkerList *SOWW_create_worker_list();
MPI_Request *SOWW_create_requests(size_t request_count);
void SOWW_Worker_add_request(struct SOWW_Worker *worker, MPI_Request *request);
MPI_Request *SOWW_Worker_get_request(struct SOWW_Worker *worker, int index);

#endif
